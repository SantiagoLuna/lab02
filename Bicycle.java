//Santiago Luna 2032367

public class Bicycle{
private String manufacturer;
private int numberGears;
private double maxSpeed;

public String getManu(){
    return this.manufacturer;
}

public int getNumGears(){
    return this.numberGears;
}

public double getMaxSpeed(){
    return this.maxSpeed;
}

public Bicycle(String man, int numGears, double ms){
    this.manufacturer = man;
    this.numberGears = numGears;
    this.maxSpeed = ms;
}

public String toString(){
    return "manufacturer: "+ getManu()+", Number of gears: "+getNumGears()+", Max Speed: "+getMaxSpeed();
}
}