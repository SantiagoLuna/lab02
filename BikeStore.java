//Santiago Luna 2032367

public class BikeStore {
    public static void main(String[] args){
        Bicycle[] bikes = new Bicycle[4];
        bikes[0] = new Bicycle("redBike", 10, 35);
        bikes[1] = new Bicycle("BlueBike", 12, 18.3);
        bikes[2] = new Bicycle("CoolBike", 20, 100);
        bikes[3] = new Bicycle("BestBikes", 2, 9.5);

        for(int i=0; i<bikes.length; i++){
            System.out.println(bikes[i]);
        }
    }
}
